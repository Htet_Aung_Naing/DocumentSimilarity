<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>
<head>
<title>Porter Stop Words</title>
 <link href="<c:url value="/resources/bootstrap.css" />" rel="stylesheet" >
    <link href="<c:url value="/resources/customize.css"/>" rel="stylesheet" >
     <link href="<c:url value="/resources/bootstrap.min.css"/>" rel="stylesheet" >
</head>
<body>
<h1 class="header">TF-IDF Result</h1>

	 <div class = "resdiv">
	
	<br/>
	<form:form method="POST" action="insert" >
	<input type="submit" class="btn btn-primary" value="Save" name = "extract" >
	<br></br>
	<table class="table table-striped">
  <thead>
    <tr>
    <th>#</th>
      <th>Word</th>
      <th>TF</th>
      <th>IDF</th>
      <th>Weight</th>
    </tr>
  </thead>
  <tbody>
			<c:forEach var="word" items="${document.wordList}" varStatus="loop">
				<tr>
				<td>${loop.index+1}.</td>
				<td>${word.word}</td>
				<td>${word.tf}</td>
				<td>${word.idf}</td>
				<td>${word.weight}</td>
				</tr>
			</c:forEach>
	</tbody>
</table>
	</form:form>
</div>
</body>
</html>