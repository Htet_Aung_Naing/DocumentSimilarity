<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>
<head>
<title>Upload File Request Page</title>
 <link href="<c:url value="/resources/bootstrap.css" />" rel="stylesheet" >
    <link href="<c:url value="/resources/customize.css"/>" rel="stylesheet" >
     <link href="<c:url value="/resources/bootstrap.min.css"/>" rel="stylesheet" >
</head>
<body>
<h1 class = "header">PDF file Upload</h1>
<h2 class="Errorheader">${message}</h2>
<h3 class="Saveheader">${savmessage}</h3>
<!-- <a href = "showList">Show Document's Word Lists</a> -->
<div class = "main">
	<form:form method="POST" action="uploadFile" enctype="multipart/form-data">
		PDF File to upload: <input type="file"  name="file"><br /> 
		<br></br>
		<input type="submit" class="btn btn-primary" value="Tokenize" >
		
	</form:form>
	 </div>
	
</body>
</html>