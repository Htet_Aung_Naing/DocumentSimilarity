<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>
<head>
<title>Porter Stop Words</title>
 <link href="<c:url value="/resources/bootstrap.css" />" rel="stylesheet" >
    <link href="<c:url value="/resources/customize.css"/>" rel="stylesheet" >
     <link href="<c:url value="/resources/bootstrap.min.css"/>" rel="stylesheet" >
</head>
<body>
<h1 class="header">Document's Word Count List</h1>

<div class="row">
	<div class="col-md-4">
	 <h3>Document 1</h3>
		 <div class = "resalldiv" >
			
		 		<table class="table table-striped">
	 		 <thead>
	   		 <tr>
	   		 <th>#</th>
	     	 <th style="width:10px;">Word</th>
	     	 <th>Count</th>
	    	</tr>
	  		</thead>
	  		 <tbody>
			<c:forEach var="word" items="${documentlst.get(0).wordList}" varStatus="loop">
				<tr>
				<td>${loop.index+1}.</td>
				<td>${word.word}</td>
				<td>${word.count}</td>
				</tr>
			</c:forEach>
			</tbody>
	  		</table>
 		</div>
	</div>
  <div class="col-md-4">
   <h3>Document 2</h3>
  			 <div class = "resalldiv" >
			
		 		<table class="table table-striped">
	 		 <thead>
	   		 <tr>
	   		 <th>#</th>
	     	 <th>Word</th>
	     	 <th>Count</th>
	    	</tr>
	  		</thead>
	  		 <tbody>
			<c:forEach var="word" items="${documentlst.get(1).wordList}" varStatus="loop">
				<tr>
				<td>${loop.index+1}.</td>
				<td>${word.word}</td>
				<td>${word.count}</td>
				</tr>
			</c:forEach>
			</tbody>
	  		</table>
 		</div>
  </div>
  <div class="col-md-4">
   <h3>Document 3</h3>
  			 <div class = "resalldiv" >
			
		 		<table class="table table-striped">
	 		 <thead>
	   		 <tr>
	   		 <th>#</th>
	     	 <th>Word</th>
	     	 <th>Count</th>
	    	</tr>
	  		</thead>
	  		 <tbody>
			<c:forEach var="word" items="${documentlst.get(2).wordList}" varStatus="loop">
				<tr>
				<td>${loop.index+1}.</td>
				<td>${word.word}</td>
				<td>${word.count}</td>
				</tr>
			</c:forEach>
			</tbody>
	  		</table>
 		</div>
  
  </div>
</div>

</body>
</html>