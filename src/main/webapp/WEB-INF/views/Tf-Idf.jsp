<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="false" %>
<html>
<head>
<title>Porter Stop Words</title>
 <link href="<c:url value="/resources/bootstrap.css" />" rel="stylesheet" >
    <link href="<c:url value="/resources/customize.css"/>" rel="stylesheet" >
     <link href="<c:url value="/resources/bootstrap.min.css"/>" rel="stylesheet" >
</head>
<body>
<h1 class="header">Porter Stemmer Word Lists</h1>
	 <div class = "resdiv">
	
	<br/>
	<form:form method="POST" action="getTFIDFWeight" >
	<input type="submit" class="btn btn-primary" value="Tf-Idf" name = "extract" >
	<br></br>
	 <ol>
			<c:forEach var="word" items="${document.words}">
				<li>${word}</li>
			</c:forEach>
	</ol>
	</form:form>
</div>
</body>
</html>