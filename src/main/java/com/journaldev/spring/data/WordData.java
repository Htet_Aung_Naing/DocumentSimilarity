package com.journaldev.spring.data;

import java.io.Serializable;

import org.springframework.stereotype.Repository;
@Repository
public class WordData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7504557010029434614L;
	int id;
	int parentId;
	String word;
	double tf;
	double idf;
	int count;
	double weight;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getParentId() {
		return parentId;
	}
	public void setParentId(int parentId) {
		this.parentId = parentId;
	}
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
	public double getTf() {
		return tf;
	}
	public void setTf(double tf) {
		this.tf = tf;
	}
	public double getIdf() {
		return idf;
	}
	public void setIdf(double idf) {
		this.idf = idf;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public WordData()
	{
		this.id = 0;
		this.idf = 0;
		this.tf = 0 ;
		this.parentId = 0;
		this.weight = 0;
		this.word = "";
		this.count = 0 ;
	}

}
