package com.journaldev.spring.impl;

import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.tartarus.snowball.ext.PorterStemmer;

import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.journaldev.spring.dao.DocumentDao;
import com.journaldev.spring.data.DocumentData;
import com.journaldev.spring.data.WordData;

public class ExtractRawDataService {

	static DecimalFormat df = new DecimalFormat("#.00"); 
	public static DocumentData extractWords(String path) {
		DocumentData res = new DocumentData();
		
		
		PdfReader reader;
		try {
			reader = new PdfReader(path);
			  int n = reader.getNumberOfPages();

			    String myLine = "";
			    // Go through all pages
			    for (int i = 1; i <= n; i++)
			    {
			      // only page number 2 will be included
			     myLine += PdfTextExtractor.getTextFromPage(reader, i);    
			    }
			    String regex = "[-+.^:,?()\"\'@]";
			    
			    if(!myLine.equals(""))
			    {			   
			    	 Pattern pattern = Pattern.compile(regex);
			    	    Matcher matcher = pattern.matcher(myLine);
			    	    myLine = matcher.replaceAll("");
			    	    
			    	ArrayList<String> wordList = new ArrayList<String>();
			    	wordList = tokenizeWithSpace(myLine);
			    	res.setWords(wordList);
			    }
			    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	     return res;
	}
	
	public static ArrayList<String> tokenizeWithSpace(String rawData)
	{
		ArrayList<String> res = new ArrayList<String>();
		rawData=rawData.trim().replaceAll("\\s+", " ");
		String[] words = rawData.split(" ");
		
		for (String word : words) 
		{
			res.add(word);
		}
		
		return res;
	}
	
	public static ArrayList<WordData> getTfScore(ArrayList<String>worList)
	{
		
		ArrayList<WordData> res = new ArrayList<WordData>();
		ArrayList<String>uniqueData = new ArrayList<String>();
		uniqueData = getUniqueData(worList);
		double tfscore = 0;
		for (String udata : uniqueData) 
		{
			WordData word = new WordData();
			 tfscore = tf(worList, udata , word);
			word.setTf(Double.valueOf(df.format(tfscore)));
			word.setWord(udata);
			res.add(word);
		}
		
		return res;
	}
	
	public static void getIdfScore(List<List<String>> docs , ArrayList<WordData>wordList)
	{
		
		double idfscore = 0;
		for (WordData word : wordList) 
		{
			idfscore = idf(docs, word.getWord());
			word.setIdf(Double.valueOf(df.format(idfscore)));
		}
		
	}
	
	public static double tf(List<String> doc, String term , WordData wordData) {
	    double result = 0;
	    for (String word : doc) {
	       if (term.equalsIgnoreCase(word))
	              result++;
	       }
	    wordData.setCount((int) result);
	    return result / doc.size();
	}
	
	public static double idf(List<List<String>> docs, String term) {
	    double n = 0;
	    double res = 0;
	    for (List<String> doc : docs) {
	        for (String word : doc) {
	            if (term.equalsIgnoreCase(word)) {
	                n++;
	                break;
	            }
	        }
	    }
	    if(n!=0)
	    	res = Math.log(docs.size() / n);
	    return res;
	}
	
	public static ArrayList<String> getUniqueData(ArrayList<String>workList)
	{
		ArrayList<String> result = new ArrayList<String>();
		Set<String> hs = new HashSet<>();
		hs.addAll(workList);
		result.addAll(hs);
		return result;
	}

	public static ArrayList<String> removeStopWords(ArrayList<String> wordsList)
	{		
		for (int i = 0; i < wordsList.size(); i++) 
		{
		
			if(!wordsList.get(i).equals("?"))
				for (int j = 0; j < DocumentData.stopWordsList.length; j++)
				{
					if (DocumentData.stopWordsList[j].contains(wordsList.get(i))) 
					{
						wordsList.remove(i);
					}
				}
			else wordsList.remove(i);
		}	
		
		return wordsList;
	}
	
	public static ArrayList<String> getPorterStemmer(ArrayList<String>wordList)
	{
		ArrayList<String> res = new ArrayList<String> ();
		
		for (String word : wordList) 
		{
			 PorterStemmer porter = new PorterStemmer();
		      porter.setCurrent(word);
		      porter.stem();
		      res.add(porter.getCurrent());
		}
		
		return res;
	}
	

	public static ArrayList<WordData> calculateTfIdfWeight(ArrayList<String> inputdoc) throws SQLException
	{
		
		ArrayList<WordData> res = new ArrayList<WordData>();
		res =  getTfScore(inputdoc);
		DocumentDao docDao = new DocumentDao();
		ArrayList<DocumentData> docList = docDao.getWordList();
		docDao.addWordList(docList);
		List<List<String>> documents = new ArrayList<List<String>>();
		for (DocumentData doc : docList) 
		{
			documents.add(doc.getWords());
		}
		getIdfScore(documents, res);
		
		for (WordData word : res)
		{
			word.setWeight(Double.valueOf(df.format(word.getIdf() * word.getTf())));
		}
	
		return res;
	}
	
	public static void getTFIDfWeight(DocumentData document) throws SQLException
	{
		document.setWordList(calculateTfIdfWeight(document.getWords()));
	}
	
	public static boolean Insert(DocumentData document) throws SQLException
	{
		boolean flag = false;
		document.setWordList(calculateTfIdfWeight(document.getWords()));
		DocumentDao docDao = new DocumentDao();
		flag = docDao.insert(document);
		return flag;
	}
	
	public static ArrayList<DocumentData> getCountwithWordList() throws SQLException
	{
		ArrayList<DocumentData> docList = new ArrayList<DocumentData>();
		DocumentDao docDao = new DocumentDao();
		docList = docDao.getWordList();
		{
			for (DocumentData documentData : docList) 
			documentData.setWordList(docDao.getWordDataListbyparenId(documentData.getId()));
		}
		return docList;
	}
	
	public static void main(String[]args)
	{
		ArrayList<String> workList = new ArrayList<String>();
		workList.add("Apple");
		workList.add("Apple");
		workList.add("orange");
		getUniqueData(workList);
	}
	
}
