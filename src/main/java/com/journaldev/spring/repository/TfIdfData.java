package com.journaldev.spring.repository;

import org.springframework.stereotype.Repository;

@Repository
public class TfIdfData {
	
	double score;
	String label;
	public double getScore() {
		return score;
	}
	public void setScore(double score) {
		this.score = score;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}

	public TfIdfData()
	{
		this.score = 0 ;
		this.label = "";
	}
	
}
