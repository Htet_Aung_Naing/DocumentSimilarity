package com.journaldev.spring.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.journaldev.spring.data.DocumentData;
import com.journaldev.spring.impl.ExtractRawDataService;
@Controller

public class FileUploadController {

	@Autowired
    ServletContext context;
	


	private static final Logger logger = LoggerFactory
			.getLogger(FileUploadController.class);
	
	
	ExtractRawDataService extractService;
	

	/**
	 * Upload single file using Spring Controller
	 */
	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	public @ResponseBody
	ModelAndView uploadFileHandler(@RequestParam("file") MultipartFile file , Model model, SessionStatus status,
			HttpServletRequest request, RedirectAttributes redirectAttributes) {
				if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				
				// Creating the directory to store file
				String rootPath = context.getRealPath("")+ File.separator + "data";
				File dir = new File(rootPath );
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				boolean flag = validateFile(dir.getAbsolutePath()+ File.separator + file.getOriginalFilename());
				
				if(flag)
				{
					File serverFile = new File(dir.getAbsolutePath()+ File.separator + file.getOriginalFilename());
					BufferedOutputStream stream = new BufferedOutputStream(
							new FileOutputStream(serverFile));
					stream.write(bytes);
					stream.close();
					
					DocumentData doc = new DocumentData();
					extractService = new ExtractRawDataService();
					doc = extractService.extractWords(rootPath+File.separator+file.getOriginalFilename());
					
					model.addAttribute("document" , doc);
					
					ModelAndView modelAndView = new ModelAndView("WhiteSpaceTokenizeWords", "document", doc);
					request.getSession().setAttribute("docu",doc);
					logger.info("Server File Location="+ serverFile.getAbsolutePath());
					 status.setComplete();
					 return modelAndView;
				}else
				{
					return new ModelAndView("upload", "message", "File is Invalid");
				}
				
			} catch (Exception e) {
				return new ModelAndView("upload", "document", "");
			}
		} else {
			//redirectAttributes.addAttribute("message","File is empty!");
			
			return new ModelAndView("upload", "message", "File is empty!");
		}
	}
	
	public boolean validateFile(String file)
	{
		boolean flag = false;
		String fileExtension = FilenameUtils.getExtension(file);
		if(fileExtension.equals("pdf"))
			flag = true;
		return flag;
	}
	
	 @RequestMapping(method = RequestMethod.GET)
	    public String setupForm(Model model)
	    {
	         return "upload";
	    }
	
	@RequestMapping(value = "/getporterStemmerWord", method = RequestMethod.POST)
	public @ResponseBody
	ModelAndView getRawDataHandler( Model model, SessionStatus status,HttpServletRequest request) {
				
		DocumentData doc = (DocumentData) request.getSession().getAttribute("removedoc");
		
		ArrayList<String> porterStemmerWords = ExtractRawDataService.getPorterStemmer(doc.getWords());
		doc.setWords(porterStemmerWords);
		request.getSession().setAttribute("porterdoc",doc);
       
		return new ModelAndView("Tf-Idf", "document", doc);
				
			} 
	
	@RequestMapping(value = "/removeStopWords", method = RequestMethod.POST)
	public @ResponseBody
	ModelAndView getRemoveStopWordsData( Model model, SessionStatus status ,HttpServletRequest request) {
		
				DocumentData doc = (DocumentData) request.getSession().getAttribute("docu");
				
				ArrayList<String> removeWordLists = ExtractRawDataService.removeStopWords(doc.getWords());
				doc.setWords(removeWordLists);
				request.getSession().setAttribute("removedoc",doc);
		       
				return new ModelAndView("RemoveStopWordsList", "docremove", doc);
						
			} 
	
	@RequestMapping(value = "/getTFIDFWeight", method = RequestMethod.POST)
	public @ResponseBody
	ModelAndView getTFIDFWeight( Model model, SessionStatus status ,HttpServletRequest request) throws SQLException {
			
				DocumentData doc = (DocumentData) request.getSession().getAttribute("porterdoc");
				request.getSession().setAttribute("tfidfweightDocu",doc);
		       ExtractRawDataService.getTFIDfWeight(doc);
				return new ModelAndView("Tf-IdfResult", "document", doc);
						
			} 
	
	@RequestMapping(value = "/showList")
	public @ResponseBody
	ModelAndView getAllDocument( Model model, SessionStatus status ,HttpServletRequest request) throws SQLException {
			
				ArrayList<DocumentData>docList = ExtractRawDataService.getCountwithWordList();
				
				return new ModelAndView("ShowWordCountList", "documentlst", docList);
						
			} 
	
	@RequestMapping(value = "/insert", method = RequestMethod.POST)
	public @ResponseBody
	ModelAndView InsertDocument( Model model, SessionStatus status ,HttpServletRequest request) throws SQLException {
		
				DocumentData doc = (DocumentData) request.getSession().getAttribute("porterdoc");
				
				/*ArrayList<String> removeWordLists = ExtractRawDataService.removeStopWords(doc.getWords());
				ArrayList<String>poterList = ExtractRawDataService.getPorterStemmer(removeWordLists);
				doc.setWords(poterList);*/
		        ExtractRawDataService.Insert(doc);
		        request.getSession().removeAttribute("porterdoc");
		        request.getSession().removeAttribute("removedoc");
		        request.getSession().removeAttribute("docu");
		        return new ModelAndView("upload", "savmessage", "Save Successfully!");
						
			} 
		
	
	public static void main(String[] args) throws IOException
	{
	
      
	}
	
	
}
