package com.journaldev.spring.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.journaldev.spring.data.DocumentData;
import com.journaldev.spring.data.WordData;

public class DocumentDao {
	
	public  boolean insert(DocumentData doc) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		boolean flag = false;
		try {
			con = DataBaseConnection.getConnection();
			     
			 query = " insert into DOCUMENT (AUTHOR_NAME, DOCUMENT_NAME ) values (?,?)";
			    	        
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, doc.getAuthorName());
				ps.setString(i++, doc.getName());
				
				ps.execute();
				
			
				int parentid = getMaxDocumentkey(con);
				flag = insertWordList(doc.getWordList(), parentid, con);
				
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("doc Registration error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public int getMaxDocumentkey(Connection con) throws SQLException
	{
		int res = 0;
		String query = "Select max(DOCUMENT_ID) from document";
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		
		if(rs.next())
		{
			res = rs.getInt(1);
					
		}
		
		return res;
	}
	
	public ArrayList<DocumentData> getWordList() throws SQLException
	{
		ArrayList<DocumentData> wordList = new ArrayList<DocumentData>();
		Connection con = null;
		String query = "Select * from DOCUMENT";
		con = DataBaseConnection.getConnection();
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 
		 while(rs.next())
		 {
			 DocumentData documentData = new DocumentData();
			 documentData.setId(rs.getInt("DOCUMENT_ID"));
			 documentData.setAuthorName(rs.getString("AUTHOR_NAME"));
			 documentData.setName(rs.getString("DOCUMENT_NAME"));
			 documentData.setWords(getWordListbyparenId(documentData.getId()));
			 
			 wordList.add(documentData);
		 }
		
		return wordList;
	}
	
	public void addWordList(ArrayList<DocumentData> docList) throws SQLException
	{
		for (DocumentData documentData : docList) 
		{
			documentData.setWords(getWordListbyparenId(documentData.getId()));
		}
	}
	
	public ArrayList<String> getWordListbyparenId(int parentid) throws SQLException
	{
		ArrayList<String> wordList = new ArrayList<String>();
		Connection con = null;
		String query = "Select * from WORD_LIST where DOC_PARENT_ID = "+parentid;
		con = DataBaseConnection.getConnection();
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 
		 while(rs.next())
		 {
			 wordList.add( rs.getString("WORD_NAME"));
			 
		 }
		
		return wordList;
	}
	
	public ArrayList<WordData> getWordDataListbyparenId(int parentid) throws SQLException
	{
		ArrayList<WordData> wordList = new ArrayList<WordData>();
		Connection con = null;
		String query = "Select * from WORD_LIST where DOC_PARENT_ID = "+parentid;
		con = DataBaseConnection.getConnection();
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 
		 while(rs.next())
		 {
			WordData word = new WordData();
			word.setCount(rs.getInt("word_count"));
			word.setWord(rs.getString("WORD_NAME"));
			 wordList.add(word);
		 }
		
		return wordList;
	}
	
	public boolean insertWordList(ArrayList<WordData> worList, int parentid , Connection con) throws SQLException
	{
		String query = "";
		PreparedStatement ps = null;
		 query = " insert into WORD_LIST (DOC_PARENT_ID, WORD_NAME, TF , IDF,WEIGHT,WORD_COUNT ) values (?, ?,? , ?,?,?)";
		for (WordData word : worList) {
			
				int i = 1;
				ps = con.prepareStatement(query);
				ps.setInt(i++, parentid);
				ps.setString(i++, word.getWord());
				ps.setDouble(i++, word.getTf());
				ps.setDouble(i++, word.getIdf());
				ps.setDouble(i++, word.getWeight());
				ps.setInt(i++, word.getCount());
			 ps.execute();
			
		}
		
		return true;
	}

}
